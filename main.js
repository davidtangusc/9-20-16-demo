function getSongs() {
  var promise = $.ajax({
    url: 'http://itp-api.herokuapp.com/songs',
    type: 'get'
  });

  var promise2 = promise.then(function(response) {
    console.log(1);
    return response.songs;
  });

  return promise2;
}

getSongs().then(function(songs) {
  console.log(2, songs);
  var templateSource = $('#song-list-template').html();
  var template = Handlebars.compile(templateSource);
  var html = template({
    favoriteSongs: songs
  });
  $('#song-list').html(html);
}, function() {
  console.log('there was an error');
});

// promise = pending, resolved (success), rejected (error)
// HTTP status code 2xx = success
// HTTP status code 4xx or 5xx = error
//
// $.ajax({
//   url: 'http://itp-api.herokuapp.com/songs',
//   type: 'get',
//   success: function(response) {
//     var templateSource = $('#song-list-template').html();
//     var template = Handlebars.compile(templateSource);
//     var html = template(response);
//     $('#song-list').html(html);
//   }
// });
//
// $.ajax({
//   url: 'http://itp-api.herokuapp.com/songs',
//   type: 'get'
// }).then(function(response) {
//   console.log(1, response);
//   return response;
// }).then(function(response) {
//   console.log(2, response);
//   return response;
// }).then(function(response) {
//   console.log(3, response);
// });
